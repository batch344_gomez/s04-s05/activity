public class Contact {
    private String name;
    private String contactNumber1;
    private String contactNumber2;
    private String homeAddress;
    private String officeAddress;

    public Contact(){};
    // Constructor for creating a contact with two contact numbers and home and office address
    public Contact(String name, String contactNumber1, String contactNumber2, String homeAddress, String officeAddress) {
        this.name = name;
        this.contactNumber1 = contactNumber1;
        this.contactNumber2 = contactNumber2;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }

    // Constructor for creating a contact with only one contact number and one address
    public Contact(String name, String contactNumber1, String homeAddress) {
        this.name = name;
        this.contactNumber1 = contactNumber1;
        this.homeAddress = homeAddress;
    }

    public String getName(){
        return this.name;
    }
    public String getContactNumber1(){
        return this.contactNumber1;
    }

    public String getContactNumber2(){
        return this.contactNumber2;
    }
    public String getHomeAddress(){
        return this.homeAddress;
    }

    public String getOfficeAddress(){
        return this.officeAddress;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }
    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }
}
