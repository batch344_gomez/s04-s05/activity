import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    // Default constructor
    public Phonebook() {
        this.contacts = new ArrayList<>();
    }

    // Parameterized constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Getter for contacts
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // Setter for contacts to add a new contact
    public void addContact(Contact contact) {
        contacts.add(contact);
    }


}
