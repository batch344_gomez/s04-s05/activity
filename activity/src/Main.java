
public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("Ian Gomez", "+639152468596", "+639228547963", "Quezon City", "Makati City");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "+639173698541", "Caloocan City", "Pasay City");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        System.out.println();
        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {

                System.out.println(contact.getName());
                System.out.println("---------------------------------");
                System.out.println(contact.getName()+ " has the following registered numbers:");
                System.out.println(contact.getContactNumber1());
                System.out.println(contact.getContactNumber2());
                System.out.println("---------------------------------");
                System.out.println(contact.getName()+ " has the following registered addresses:");
                System.out.println("my home in " + contact.getHomeAddress());
                System.out.println("my office in " + contact.getOfficeAddress());
                System.out.println("=================================");
            }
        }
    }
}